where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

ASYN = asyn

asyn_VERSION:=$(ASYN_DEP_VERSION)
calc_VERSION:=$(CALC_DEP_VERSION)

DEVEPICS:=$(ASYN)/devEpics

USR_CFLAGS   += -Wno-unused-variable
USR_CFLAGS   += -Wno-unused-function
USR_CFLAGS   += -Wno-unused-but-set-variable 
USR_CFLAGS += -DHAVE_CALCMOD

DBDS += $(DEVEPICS)/asynCalc.dbd

SOURCES   += $(DEVEPICS)/devAsynOctet.c
